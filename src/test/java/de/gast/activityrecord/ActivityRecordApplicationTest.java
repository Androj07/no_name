package de.gast.activityrecord;

import de.gast.activityrecord.audith.repository.DeleteAuditRepository;
import de.gast.activityrecord.entity.Activity;
import de.gast.activityrecord.entity.Route;
import de.gast.activityrecord.repository.ActivityRepository;
import de.gast.activityrecord.repository.RouteRepository;
import io.restassured.RestAssured;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
public class ActivityRecordApplicationTest {

    @Value("${server.contextPath}")
    private String contextPath;

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private DeleteAuditRepository deleteAuditRepository;

    @Test
    public void givenValidParametersShouldCreateActivity() {
        String sessionId = "sessionId";
        String clientIp = "clientIp";
        String domain = "domain";

        String path = "path";
        String hostName = "hostName";
        String hostIp = "hostIp";
        RestAssured
                .given()
                .port(9090)
                .basePath(contextPath)
                .param("sessionId", sessionId)
                .param("clientIp", clientIp)
                .param("domain", domain)
                .param("path", path)
                .param("hostName", hostName)
                .param("hostIp", hostIp)
                .when()
                .get("save")
                .then()
                .assertThat()
                .statusCode(200);

        Activity activity = activityRepository.findBySessionIdAndClientIp(sessionId, clientIp);
        assertNotNull(activity);
    }

    @Test
    public void givenInvalidParametersShouldNotCreateActivity() {
        String sessionId = "sessionId";
        String clientIp = "clientIp";
        String invalidValue = null;

        String path = "path";
        String hostName = "hostName";
        String hostIp = "hostIp";
        RestAssured
                .given()
                .port(9090)
                .basePath(contextPath)
                .param("sessionId", sessionId)
                .param("clientIp", clientIp)
                .param("domain", invalidValue)
                .param("path", path)
                .param("hostName", hostName)
                .param("hostIp", hostIp)
                .when()
                .get("save")
                .then()
                .assertThat()
                .statusCode(200);

        Activity activity = activityRepository.findBySessionIdAndClientIp(sessionId, clientIp);
        assertNull(activity);
    }

    @Test
    public void deleteShouldRemoveActivityIfLastIsBeforeGivenDate() throws Exception {
        //given
        String sessionId = "sessionId";
        String clientIp = "clientIp";
        String domain = "domain";

        String path = "path";
        String hostName = "hostName";
        String hostIp = "hostIp";

        Date tomorrow = Date.from(ZonedDateTime.now().plusDays(1).toInstant());
        DateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");
        String formattedDate = formatter.format(tomorrow);

        Activity deleteCandidate = createTestActivity(sessionId, clientIp, new Date(), 1);
        Route routeToDelete = createTestRoute(deleteCandidate, domain, hostIp, hostName, path, new Date());

        Activity activityToStay = createTestActivity(sessionId, clientIp, tomorrow, 1);
        Route routeToStay = createTestRoute(activityToStay, domain, hostIp, hostName, path, tomorrow);



        //when
        RestAssured
                .given()
                .port(9090)
                .log().all()
                .basePath(contextPath)
                .param("domain", domain)
                .param("date", formattedDate)
                .when()
                .delete("delete")
                .then()
                .assertThat()
                .statusCode(200);
        //then
        assertFalse(routeRepository.exists(routeToDelete.getId()));
        assertFalse(activityRepository.exists(deleteCandidate.getId()));
        assertTrue(routeRepository.exists(routeToStay.getId()));
        assertTrue(activityRepository.exists(activityToStay.getId()));
    }

    @After
    public void cleanUp() {
        System.out.println("====== DELETE AUDIT =====");
        deleteAuditRepository.findAll().forEach(System.out::println);

        System.out.println("=========================");
        routeRepository.deleteAll();
        activityRepository.deleteAll();
    }

    private Activity createTestActivity(String sessionId, String clientIp, Date lastDate, Integer counter) {
        Activity activity = new Activity();
        activity.setSessionId(sessionId);
        activity.setClientIp(clientIp);
        activity.setCounter(counter);
        activity.setLast(lastDate);
        return activityRepository.save(activity);
    }

    private Route createTestRoute(Activity activity, String domain, String hostIp, String hostName, String path, Date currentDate) {
        Route route = new Route();
        route.setActivity(activity);
        route.setDomain(domain);
        route.setHostIp(hostIp);
        route.setHostName(hostName);
        route.setPath(path);
        route.setTimeStamp(currentDate);
        return routeRepository.save(route);
    }

}