package de.gast.activityrecord.audith;

public interface ISoutLogger {

    public void log(String message);
}
