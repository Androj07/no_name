package de.gast.activityrecord.audith.repository;

import de.gast.activityrecord.audith.entity.DeleteAudit;
import org.springframework.data.repository.CrudRepository;

public interface DeleteAuditRepository extends CrudRepository<DeleteAudit,Long> {
}
