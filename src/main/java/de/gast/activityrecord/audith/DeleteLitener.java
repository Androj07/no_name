package de.gast.activityrecord.audith;

import de.gast.activityrecord.configuration.SpringApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.persistence.PostRemove;
import javax.persistence.PreRemove;


public class DeleteLitener {


    @PostRemove
    public void onDelete(Object deleted){
        ISoutLogger logger = SpringApplicationContext.getBean(ISoutLogger.class);
        logger.log("++++++++ wyjebałem "+deleted.toString());
    }

    @PreRemove
    public void preDestroy(Object toDelete){
        ISoutLogger logger = SpringApplicationContext.getBean(ISoutLogger.class);
        logger.log("-------- zaraz wyjebie "+ toDelete.toString());
    }

}
