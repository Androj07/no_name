package de.gast.activityrecord.audith.entity;

import org.springframework.context.ApplicationEvent;

import java.util.Date;

public class DeleteEvent extends ApplicationEvent{
    private final String description;
    private final Date deleteDate;

    public DeleteEvent(Object source, String description, Date deleteDate) {
        super(source);
        this.description = description;
        this.deleteDate = deleteDate;
    }

    public String getDescription() {
        return description;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }
}
