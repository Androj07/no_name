package de.gast.activityrecord.audith.controller;


import de.gast.activityrecord.audith.entity.DeleteAudit;
import de.gast.activityrecord.audith.entity.DeleteEvent;
import de.gast.activityrecord.audith.repository.DeleteAuditRepository;
import org.hibernate.event.spi.PostDeleteEvent;
import org.hibernate.event.spi.PostDeleteEventListener;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Controller;

import java.util.Date;

@Controller
public class DeleteAuditController implements PostDeleteEventListener{

    private final DeleteAuditRepository deleteAuditRepository;

    @Autowired
    public DeleteAuditController(DeleteAuditRepository deleteAuditRepository) {
        this.deleteAuditRepository = deleteAuditRepository;
    }


    @Override
    public void onPostDelete(PostDeleteEvent postDeleteEvent) {
        DeleteAudit deleteAudit = new DeleteAudit();
        deleteAudit.setDescription(postDeleteEvent.getEntity().toString());
        deleteAudit.setDeleteDate(new Date());
        deleteAuditRepository.save(deleteAudit);
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }
}
