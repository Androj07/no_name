package de.gast.activityrecord;

import de.gast.activityrecord.audith.DeleteLitener;
import de.gast.activityrecord.audith.ISoutLogger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@SpringBootApplication
public class ActivityRecordApplication extends SpringBootServletInitializer {
    
    public static void main(String[] args) throws Exception {
	SpringApplication.run(ActivityRecordApplication.class, args);
    }

    @Bean
    public ISoutLogger iSoutLogger(){
        return System.out::println;
    }
}