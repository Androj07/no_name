package de.gast.activityrecord.repository;

import de.gast.activityrecord.entity.Activity;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface ActivityRepository  extends CrudRepository<Activity, Integer>{

    Activity findBySessionIdAndClientIp(String sessionId, String clientIp);

    void deleteByIdIn(Set<Long> idsToDelete);
}
