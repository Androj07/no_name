package de.gast.activityrecord.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.gast.activityrecord.entity.Route;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface RouteRepository extends CrudRepository<Route, Integer>{

    List<Route> findByDomainAndTimeStampBefore(String domain,Date timestamp);

    @Query("SELECT r.activity.id FROM Route r where r.activity.last < :date AND r.domain = :domain")
    Set<Long> findActivityIdWhereActivityLastBeforeDateAndRouteDomainEquals(@Param("date") Date date, @Param("domain") String domain);

    @Query("DELETE FROM Route r WHERE r.activity.id in ?1")
    @Modifying
    void deleteAllByActivity_Id(@Param("activityIds") Set<Long> activityIds);
}
