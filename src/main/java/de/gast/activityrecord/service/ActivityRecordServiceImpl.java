package de.gast.activityrecord.service;

import java.time.Instant;
import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.gast.activityrecord.entity.Activity;
import de.gast.activityrecord.entity.Route;
import de.gast.activityrecord.repository.ActivityRepository;
import de.gast.activityrecord.repository.RouteRepository;

import javax.validation.constraints.NotNull;

@Service
public class ActivityRecordServiceImpl implements ActivityRecordService {

    private final ActivityRepository activityRepository;
    private final RouteRepository routeRepository;

    @Autowired
    public ActivityRecordServiceImpl(ActivityRepository activityRepository, RouteRepository routeRepository) {
        this.activityRepository = activityRepository;
        this.routeRepository = routeRepository;
    }

    @Override
    @Transactional
    public void saveActivityRecord(String sessionId, String clientIp, String domain, String path,
                                   String hostName, String hostIp) {

        Date currentDate = new Date();

        Activity activity = createOrUpdateActivity(sessionId, clientIp, currentDate);

        Route route = new Route();
        route.setActivity(activity);
        route.setDomain(domain);
        route.setHostIp(hostIp);
        route.setHostName(hostName);
        route.setPath(path);
        route.setTimeStamp(currentDate);

        routeRepository.save(route);
    }


    @Override
    @Transactional
    public void deleteActivityRecord(@NotNull String domain, @NotNull Date date) {
        Set<Long> activitiesToDelete = routeRepository.findActivityIdWhereActivityLastBeforeDateAndRouteDomainEquals(date, domain);
        routeRepository.deleteAllByActivity_Id(activitiesToDelete);
        activityRepository.deleteByIdIn(activitiesToDelete);
    }

    private Activity createOrUpdateActivity(String sessionId, String clientIp, Date currentDate) {
        Activity activity = activityRepository.findBySessionIdAndClientIp(sessionId, clientIp);
        if (activity == null) {
            activity = new Activity();
            activity.setBegin(currentDate);
        }
        setActivityRecordMainData(sessionId, clientIp, activity, currentDate);
        activityRepository.save(activity);
        return activity;
    }

    private void setActivityRecordMainData(String sessionId, String clientIp, Activity activity, Date currentDate) {
        activity.setSessionId(sessionId);
        activity.setClientIp(clientIp);
        activity.setCounter(activity.getCounter() + 1);
        activity.setLast(currentDate);
    }
}
